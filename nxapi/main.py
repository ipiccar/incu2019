from nxostoolkit import Nexus

nexus = Nexus()

nexus.authenticate("admin", "wrong_passw0rd") #trying to authenticate with wrong credentials
nexus.authenticate("admin", "Admin_1234!")

nexus.get_interace_status("Eth1/1")
nexus.get_interace_status("Ath1/1") #typo in the name of the interface
nexus.get_interace_status("Ethernet1/2")
nexus.get_interace_status("Ethernet 1/3")

nexus.configure_interface_desc("Eth1/1", "very useful description of Eth1/1")