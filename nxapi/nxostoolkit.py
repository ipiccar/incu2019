import requests
import json

class Nexus:
    """
    A class used to communicate with a Nexus switch

    Attributes
    ----------
    version : str
        the version running on the switch

    platform : str
         the model of the switch

    Methods
    -------
    authenticate()
        used to authenticate to the switch
    get_interface_status(if_name)
        returns the status of the specified interface
    configure_interface_descr(if_name, description)
        configures the description of the specified interface
    """

    version: str
    platform: str

    def __init__(self):
        self.__online_nexus = {"ip": "sbx-nxos-mgmt.cisco.com",
               "port": "80",
               "user":"",
               "pass":""}
        self.__uri = 'http://{}:{}/ins'.format(self.__online_nexus['ip'],
                                         self.__online_nexus['port'])
        self.__jsonrpc_headers = {'Content-Type': 'application/json-rpc'}

    def authenticate(self, user: str, password: str):
        """ Authenticates to the switch

        Parameters
        ----------
        user : str
        password : str

        """

        self.__online_nexus["user"]=user
        self.__online_nexus["pass"]=password
        self.__get_version_and_platform()

    def get_interace_status(self, if_name: str):
        """ Returns the status of the specified interface

        Parameters
        ----------
        if_name : str
            name of the specified interface

        """

        payload = [
          {
            "jsonrpc": "2.0",
            "method": "cli",
            "params": {
              "cmd": "show interface {}".format(if_name) ,
              "version": 1
            },
            "id": 1
          }
        ]
        response = self.__send_request(payload)
        if(response.status_code==200):
            if "error" in response.json():
                status = response.json()["result"]["body"]["TABLE_interface"]["ROW_interface"]["state"]
                print("Status : ", status)
        else:
            print(response.reason)
            if "error" in response.json():
                print(json.dumps(response.json()["error"]["message"], indent=4))
                print(json.dumps(response.json()["error"]["data"]["msg"], indent=4))

    def configure_interface_desc(self, if_name: str, description: str):
        """ Returns the status of the specified interface

        Parameters
        ----------
        if_name : str
            name of the specified interface
        description : str
            description for the interface

        """

        payload = [
          {
            "jsonrpc": "2.0",
            "method": "cli",
            "params": {
              "cmd": "configure terminal",
              "version": 1
            },
            "id": 1
          },
          {
            "jsonrpc": "2.0",
            "method": "cli",
            "params": {
              "cmd": "interface {}".format(if_name) ,
              "version": 1
            },
            "id": 1
          },
          {
            "jsonrpc": "2.0",
            "method": "cli",
            "params": {
              "cmd": "description {}".format(description) ,
              "version": 1
            },
            "id": 1
          }
        ]
        response = self.__send_request(payload)
        if(response.status_code==200):
            print("Description set to : ", description)
        else:
            print(response.reason)
            if "error" in response.json():
                print(json.dumps(response.json()["error"], indent=4))

    def __get_version_and_platform(self):
        payload = [
          {
            "jsonrpc": "2.0",
            "method": "cli",
            "params": {
              "cmd": "show version",
              "version": 1
            },
            "id": 1
          }
        ]
        response = self.__send_request(payload)
        if(response.status_code==200):
            version = json.dumps(response.json()["result"]["body"], indent=4)
            platform = response.json()["result"]["body"]["chassis_id"]
            print("Version : ", version)
            print("Platform : ", platform) 
        else:
            print(response.reason)
            
        

    def __send_request(self, payload):
        response = requests.post(self.__uri, 
                         data=json.dumps(payload),
                         headers=self.__jsonrpc_headers, 
                         auth=(self.__online_nexus["user"], self.__online_nexus["pass"]))
        return response          
